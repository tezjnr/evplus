//
//  ViewController.swift
//  evPlus
//
//  Created by Terry Gillespie on 13/12/2019.
//  Copyright © 2019 isotype. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

struct Charger: Codable {
    let AddressInfo: MyAddressInfo
}
struct MyAddressInfo: Codable {
    let Title: String
    let AddressLine1: String
    let Latitude: Double
    let Longitude: Double
}



class ViewController: UIViewController {
    

    @IBOutlet weak var mapView: MKMapView!
    
    // set initial radius in metres
    let regionRadius: CLLocationDistance = 10000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set initial location in Naples
        let initialLocation = CLLocation(latitude: 40.8518, longitude: 14.2681)
        // set the center radius
        centerMapOnLocation(location: initialLocation)
        // add my pin on a map with annotation
//        addMyPin()

        
        fetchChargesJSON { (res) in
            switch res {
            case .success(let charges):
                charges.forEach({ (charge) in
                    
                    self.addMyPin(myLat:(charge.AddressInfo.Latitude),myLon:(charge.AddressInfo.Longitude),myTitle: (charge.AddressInfo.Title) )
    
                    print((charge.AddressInfo.Latitude),(charge.AddressInfo.Longitude))

//                    func addMyPin () {
//                        let annotation = MKPointAnnotation()
//                        let myCoordinates = CLLocationCoordinate2D(latitude: (charge.AddressInfo.Latitude), longitude: (charge.AddressInfo.Longitude))
//                        annotation.coordinate = myCoordinates
//                        annotation.title = ("\(charge.AddressInfo.Title)")
//                        self.mapView.addAnnotation(annotation)
//                    }
                    
                    
                })
            case .failure(let err):
                print("Failed to fetch charges:", err)
            }
            
        }

    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
      mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func addMyPin (myLat:Double, myLon:Double, myTitle:String) {
        let annotation = MKPointAnnotation()
        let myCoordinates = CLLocationCoordinate2D(latitude: myLat, longitude: myLon)
        annotation.coordinate = myCoordinates
        annotation.title = myTitle
        mapView.addAnnotation(annotation)
    }
    
    fileprivate func fetchChargesJSON(completion: @escaping (Result<[Charger], Error>) -> ()) {
        
        let urlString = "https://api.openchargemap.io/v3/poi/?output=json&countrycode=it&maxresults=3000&compact=true&verbose=false"
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            
            if let err = err {
                completion(.failure(err))
                return
            }
            
            // successful
            do {
                let charges = try JSONDecoder().decode([Charger].self, from: data!)
                completion(.success(charges))
//                completion(charges, nil)
                
            } catch let jsonError {
                completion(.failure(jsonError))
//                completion(nil, jsonError)
            }
            
            
        }.resume()
    }

}

